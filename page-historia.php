<?php
/*
Template Name: História
*/
?>

<?php get_header(); ?>
      <div class="bar"></div>
      <?php wp_nav_menu( array( 'theme_location' => 'menu-historia' ) ); ?>
      <main class="content-area">
        <?php
          while( have_posts() ): the_post();

            get_template_part( 'includes/post/content',  'page' );

            if ( comments_open() || get_comments_number() ) :
      				comments_template();
      			endif;

          endwhile;
        ?>
      </main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
