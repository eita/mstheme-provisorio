<?php
/*
  # The Index Template
    The most generic template file. It puts together the home page of the site and every other page that hasn't an template attached to it.
*/
get_header(); ?>

      <main class="content-area">

        <?php if( have_posts() ) : ?>

          <div class="posts row">
            <?php while( have_posts() ): the_post();

              get_template_part( 'includes/post/blog', get_post_format() );

            endwhile; ?>
            <?= do_shortcode('[ajax_load_more id="6256727418" post_type="post" posts_per_page="10" offset="20" button_label="Mais antigos" button_loading_label="Carregando..."]'); ?>
          </div>

        <?php else:

          # If there isn't any posts, we will just show a message.
          get_template_part( 'includes/post/content', 'none' );

        endif; ?>
      </main>


<?php get_footer(); ?>
