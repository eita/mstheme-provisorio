<?php
function my_theme_enqueue_styles() {

    $parent_style = 'eliza-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function register_menu_historia() {
  register_nav_menu('menu-historia',__( 'Menu História' ));
}
add_action( 'init', 'register_menu_historia' );

// function custom_rewrite_tag() {
//   add_rewrite_tag('%cat_slug%', '([^&]+)');
// }
// add_action('init', 'custom_rewrite_tag', 10, 0);
//
// function custom_rewrite_rule() {
//   add_rewrite_rule('^noticias/([^/]*)/?','index.php?page_id=192593&cat_slug=$matches[1]','top');
// }
// add_action('init', 'custom_rewrite_rule', 10, 0);


function the_cat_image( $term_id ){
  $wplp_image = get_option('wplp_category_image');

  if (!empty($wplp_image)) {
      foreach ($wplp_image as $val) {
          if ($term_id === $val->term_id) {
              return $val->image;
          }
      }
  }

}

add_theme_support( 'post-formats', array( 'video' ) );

function mst_get_capa (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 1,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'cover',
        ),
    ),
  );
  $capa = new WP_Query( $args );

  $capa->the_post();

  $out = "<h1><a href='".get_the_permalink()."'>" . get_the_title() . "</a></h1>";
  $out .= "<p>" . get_the_excerpt() . "</p>";
  return $out;

}

function mst_get_subcapa (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 1,
    'offset' => 5,
    'tax_query' => array(
  	'operator' => 'AND',
          array(
              'taxonomy' => 'category',
              'field'    => 'slug',
              'terms'    => 'tv',
              'operator' => 'NOT IN',
          ),
          array(
              'taxonomy' => 'category',
              'field'    => 'slug',
              'terms'    => 'cover',
              'operator' => 'NOT IN',
          ),
          array(
              'taxonomy' => 'category',
              'field'    => 'slug',
              'terms'    => 'featured-news',
              'operator' => 'NOT IN',
          ),
      ),
  );
  $capa = new WP_Query( $args );

  $capa->the_post();

  $out = "<h1><a href='".get_the_permalink()."'>" . get_the_title() . "</a></h1>";
  $out .= "<p>" . get_the_excerpt() . "</p>";
  return $out;
}

function mst_get_destaque (){
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 200,
    'offset' => 0,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'cover',
	    'operator' => 'NOT IN'
        ),
    ),
  );
  $query = new WP_Query( $args );

  echo '<div class="posts row">';
  $i = 0;
  while( $query->have_posts() ): $query->the_post();
      if (has_term('featured-news', 'category')){
        get_template_part( 'includes/post/blog', get_post_format() );
        $i++;
        if ($i>2) break;
      }
  endwhile;
  echo '</div>';

}

function mst_get_featured_video (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 1,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'tv',
        ),
    ),
  );
  $query = new WP_Query( $args );
  $query->the_post();
  $url = get_the_content();
  $url = strip_tags($url);
  $htmlcode = wp_oembed_get($url);
  if(!$htmlcode) {
  	$htmlcode = get_the_post_thumbnail();
  }

  ?>
<article class="headline" itemprop="video" itemscope="" itemtype="http://schema.org/VideoObject">
    <?= $htmlcode ?>
    <?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
  </article>
  <?php
}

function mst_get_videos (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'offset' => 1,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'tv',
        ),
    ),
  );
  $query = new WP_Query( $args );
  ?>
  <aside class="videos">
    <ul>
  <?php
    while( $query->have_posts() ): $query->the_post();
  ?>
      <li>
        <a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
      </li>
  <?php endwhile; ?>
    </ul>
  </aside><?php
}

function mst_get_especiais (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 1,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'special-stories',
        ),
    ),
  );
  $query = new WP_Query( $args );

  while( $query->have_posts() ): $query->the_post();
      get_template_part( 'includes/post/blog', get_post_format() );
  endwhile;

}

function mst_get_opiniao (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'articles',
        ),
    ),
  );
  $query = new WP_Query( $args );

  while( $query->have_posts() ): $query->the_post();
      get_template_part( 'includes/post/article', get_post_format() );
  endwhile;

}

function mst_get_entrevistas (){
  # capa
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'interviews',
        ),
    ),
  );
  $query = new WP_Query( $args );

  $query->the_post();
  echo '<a href="' . esc_url( get_permalink() ) . '">';
  echo '<figure style="background-image:url(\'' . get_the_post_thumbnail_url() . '\');"><figcaption>' . get_the_title( ) . '</figcaption></figure></a>';

  $query->the_post();
  the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );

  $query->the_post();
  the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );

}

function migrate_categories() {
  $cats_to_migrate = array(
'album' => 4199,
'articles' => 4189,
'boletim' => 4202,
'campaign' => 4104,
'cover' => 4192,
'culture' => 4197,
'education' => 4196,
'featured-news' => 4188,
'interviews' => 4191,
'mass-front' => 4198,
'musicoteca' => 4200,
'partner' => 4201,
'production' => 4108,
'special-stories' => 4193,
'tape' => 4203,
'tv' => 4195,
);

  foreach ($cats_to_migrate as $cat => $cat_id) {
    $args = array(
      'post_type' => 'post',
      'posts_per_page' => 7000,
      'tax_query' => array(
          array(
              'taxonomy' => 'category',
              'field'    => 'slug',
              'terms'    => $cat,
          ),
      ),
    );
    $query = new WP_Query( $args );
    while ($query->have_posts()): $query->the_post();
      wp_set_post_terms( get_the_ID(), $cat, 'category', true );
    endwhile;
  }
}


function set_ytbvideo_image($post_id) {

    if ( $parent_id = wp_is_post_revision( $post_id ) )
        $post_id = $parent_id;

    if ( has_term( 'tv', 'category', $post_id ) ){
        if (!has_post_thumbnail( $post_id )) {
            $url = trim(strip_tags(get_the_content( NULL, NULL, $post_id )));

            preg_match('/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.[a-zA-Z]{1,3}\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\”‘>]+)/', $url, $output_array);

            if ($output_array != array()) {

                $img = "https://img.youtube.com/vi/" . $output_array[1] . "/sddefault.jpg";

                $upload = wp_upload_bits(get_the_title( $post_id ) . ".jpg", null, file_get_contents($img));

                $filename = $upload['file'];
                $wp_filetype = wp_check_filetype($filename, null );
                $attachment = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => sanitize_file_name($filename),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );
                set_post_thumbnail( $post_id, $attach_id );
            }
        }
    }
}
add_action( 'save_post', 'set_ytbvideo_image' );


?>
