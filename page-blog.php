<?php
/*
Template Name: Blog

*/
get_header();
// migrate_categories();
# capa
$args = array(
  'post_type' => 'post',
  'posts_per_page' => 100,
);
$query = new WP_Query( $args );

?>

      <main class="content-area">

        <?php if( $query->have_posts() ) : ?>
          <header class="page-header">
  					<h1>Todas as notícias</h1>
    			</header><!-- .page-header -->

          <div class="posts row">
            <?php while( $query->have_posts() ): $query->the_post();

              get_template_part( 'includes/post/blog', get_post_format() );

            endwhile; ?>
          </div>

          <?php the_posts_navigation(); ?>

        <?php else:

           # If there isn't any posts, we will just show a message.
           get_template_part( 'includes/post/content', 'none' );

        endif; ?>
      </main>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
