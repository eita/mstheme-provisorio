<?php
/*
  # The Index Template
    The most generic template file. It puts together the home page of the site and every other page that hasn't an template attached to it.
*/
get_header();

?>

      <main class="content-area">
        <div class="row">
          <div class="col-sm-5">
            <?php echo do_shortcode('[smartslider3 slider=2]'); ?>
          </div>
          <div class="col-sm-7">
            <div class="capa">
              <?= mst_get_capa(); ?>
            </div>
            <div class="subcapa">
              <?= mst_get_subcapa(); ?>
              <p class="more_link"><a href="/noticias">Mais notícias...</a></p>
            </div>
          </div>

        </div>
        <div class="home row">
          <h2>Destaques</h2>
          <?php mst_get_destaque(); ?>
        </div>
        <div class="home row">
          <div class="col-sm-8">
            <h2><a href="/videos">Vídeos</a></h2>
            <?php mst_get_videos(); ?><?php mst_get_featured_video(); ?>
          </div>
          <div class="col-sm-4">
            <h2>Reportagens Especiais</h2>
            <?php mst_get_especiais(); ?>
            <p class="more_link"><a href="/secao/special-stories/">Mais...</a></p>
          </div>
        </div>
        <div class="home row">
          <div class="col-sm-8">
            <h2>Opinião</h2>
            <?php mst_get_opiniao(); ?>
          </div>
          <div class="col-sm-4 interviews">
            <h2>Entrevistas</h2>
            <?php mst_get_entrevistas(); ?>
          </div>
        </div>
        <div class="home row">
          <section class="newspaper col-sm-4">
            <figure>
              <a href="https://www.mst.org.br/2014/12/05/jornal-sem-terra-326" class="headline-text-over-image" title="Jornal sem terra Novembro">
                <img src="https://mst.org.br/wp-content/uploads/2019/11/15764844520_5338b2f862_b.jpg" alt="Jornal Sem Terra">
                <figcaption>Jornal Sem Terra #326</figcaption>
              </a>
            </figure>
          </section>
          <section class="facebook col-sm-4">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FMovimentoSemTerra&amp;width&amp;height=400&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=784439748288439" scrolling="no" style="border:none; overflow:hidden; height:400px;" allowtransparency="true" frameborder="0"></iframe>
          </section>
          <section class="twitter col-sm-4">
            <a class="twitter-timeline" data-height="550" href="https://twitter.com/MST_Oficial?ref_src=twsrc%5Etfw">Tweets by MST_Oficial</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </section>
        </div>
      </main>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
