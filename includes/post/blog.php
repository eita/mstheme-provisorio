
<article id="post-<?php the_ID(); ?>" class="col">
  <header class="entry-header">
      <?php if( has_post_thumbnail() ): ?>
        <div class="post-thumbnail">
          <?php eliza_post_thumbnail(); ?>
        </div>
      <?php endif;?>
      <?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>

    <?php eliza_posted_on(); ?>

  </header>

  <div class="entry-content">
      <span class="grid-excerpt"><?= strip_tags(get_the_excerpt()); ?></span>
  </div>
</article>
