<?php if( is_singular() ): ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php else: ?>
<article id="post-<?php the_ID(); ?>" class="col">
<?php endif ?>
  <header class="entry-header">

    <?php if( is_sticky() ): ?>
      <a href="<?php the_permalink(); ?>" rel="bookmark" class="sticky-star">
        <?php echo eliza_get_genericon( 'star', esc_html__( 'Featured', 'eliza' ) ); ?>
      </a>
    <?php endif; ?>

    <?php if( is_singular() ): ?>
      <?php
      echo get_the_tag_list('<div class="tags">', NULL, '</div>');
      ?>
      <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
      <span class="excerpt"><?php the_excerpt(); ?></span>
    <?php else: ?>
      <?php if( has_post_thumbnail() ): ?>
        <div class="post-thumbnail">
          <?php eliza_post_thumbnail(); ?>
        </div>
      <?php endif;?>
      <?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
    <?php endif; ?>


    <?php eliza_posted_on(); ?>

    <?php //eliza_byline(); ?>
  </header>

  <div class="entry-content">
    <?php if( is_singular() ): ?>
      <?php the_content( esc_html__( 'Continue reading&hellip;', 'eliza' ) ); ?>
      <?= do_shortcode('[frontpage_news widget="131108" name="Última notícias"]'); ?>
    <?php else: ?>
      <?php the_excerpt(); ?>
    <?php endif; ?>
  </div>
</article>
