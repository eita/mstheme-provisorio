
<article id="post-<?php the_ID(); ?>" class="col article">
  <header class="entry-header">
      <?php if( has_post_thumbnail() ): ?>
        <div class="post-thumbnail">
          <?php eliza_post_thumbnail(); ?>
        </div>
      <?php endif;?>
  </header>

  <div class="entry-content">
      <?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
      <?php the_excerpt(); ?>
  </div>
</article>
