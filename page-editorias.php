<?php
/*
Template Name: Noticias seção

*/
get_header();

global $wp_query;
$cat_slug = $wp_query->query_vars['cat_slug'];

$term = get_term_by( 'slug', $cat_slug, 'secao' );

$args = array(
  'post_type' => 'post',
  'posts_per_page' => 100,
  'tax_query' => array(
      array(
          'taxonomy' => 'secao',
          'field'    => 'slug',
          'terms'    => $cat_slug,
      )
  )
);
$query = new WP_Query( $args );

?>

      <main class="content-area">

        <?php if( $query->have_posts() ) : ?>
          <header class="page-header">
  					<h1>Notícias - <?= $term->name ?></h1>
    			</header><!-- .page-header -->

          <div class="posts row">
            <?php while( $query->have_posts() ): $query->the_post();

              get_template_part( 'includes/post/blog', get_post_format() );

            endwhile; ?>
          </div>

          <?php the_posts_navigation(); ?>

        <?php else:

           # If there isn't any posts, we will just show a message.
           get_template_part( 'includes/post/content', 'none' );

        endif; ?>
      </main>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
