<?php
/*
  # The Index Template
    The most generic template file. It puts together the home page of the site and every other page that hasn't an template attached to it.
*/
get_header(); ?>

      <main class="content-area">
        <?php if( have_posts() ) : ?>
          <header class="page-header">
            <?php
              $image = the_cat_image( get_queried_object()->term_id );
              if($image): ?>
              <figure class="category_image">
                <img src="<?= $image ?>" />
              </figure>
            <?php endif ?>
    				<?php
    					// the_archive_title( '<h1 class="page-title">', '</h1>' );
              echo "<h1>" . single_cat_title( '', false ) . "</h1>";
    					the_archive_description( '<div class="archive-description">', '</div>' );

    				?>

    			</header><!-- .page-header -->
          <h2 class="hat">Notícias</h2>
          <div class="posts row">
            <?php
              $i = 0;
              while( have_posts() ): the_post();

              get_template_part( 'includes/post/content', get_post_format() );
              if ($i++ > 2) break;

            endwhile; ?>
          </div>

          <?php
          $args = array(
            'post_type' => 'post',
            'posts_per_page' => '3',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'secao',
                    'field'    => 'slug',
                    'terms'    => 'tv',
                ),
                array(
                    'taxonomy' => 'category',
                    'field'    => 'term_id',
                    'terms'    => get_queried_object()->term_id,
                ),
            ),
          );
          $video_query = new WP_Query( $args );
          ?>
          <h2 class="hat">Vídeos</h2>
          <div class="videos row">

          <?php
            while ( $video_query->have_posts() ) {
                $video_query->the_post();
                get_template_part( 'includes/post/content', get_post_format() );
            }

            wp_reset_postdata();
          ?>
          </div>
          <p class="more_link"><a href="/videos">Todos os vídeos</a></p>


          <?php //the_posts_navigation(); ?>

        <?php else:

           # If there isn't any posts, we will just show a message.
           get_template_part( 'includes/post/content', 'none' );

        endif; ?>
      </main>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
